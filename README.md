# shard-fd

A bacula plugin that allows splitting a job into a number of smaller jobs, distributing files in a deterministic manner based on a hash of the full file path or the containing directory path. With databases this is called sharding so I stole the name.

## Compilation & Installation

*In examples my working directory is ~/src/shard and the bacula source is in ~/src/bacula-9.0.6*

1. Obtain and extract the bacula source. If you're using a packaged bacula-fd it's probably best it matches. Under Debian/Ubuntu `apt source bacula-client` will download and extract the correct version in the current working directory.
2. Configure it. In the top level of the bacula source directory run: `./configure --enable-client-only`
3. *Optionally,* modify bacula so estimates call option plugins
 1. Patch Bacula's `src/filed/estimate.c`

            $ make BACDIR=~/src/bacula-9.0.6/ patch-estimate
 2. Make and install bacula as appropriate for your Linux distribution [Debian Docs](https://www.debian.org/doc/manuals/maint-guide/build.en.html)

4. Make the plugin, defining BACDIR and PLUGINDIR. For example, if I extracted bacula to ~/src on Ubuntu 18.04: 

        $ make BACDIR=~/src/bacula-9.0.6/ PLUGINDIR=/usr/lib/bacula
5. If there are no errors then install the plugin (you'll probably have to be root for this):

        $ sudo make BACDIR=~/src/bacula-9.0.6/ PLUGINDIR=/usr/lib/bacula install

## Configuration

You will need to set `Plugin Directory` in your `bacula-fd.conf` and adjust the `bacula-dir.conf` by creating a set of filesets with the plugin command in an options section. 

The plugin command is broken down into 4 parts:
1. The plugin name, shard
2. The bucket for this fileset, integer starting at zero
3. The total number of buckets, integer
4. Whether to hash the whole pathname (integer != 1) or the containing directory name (integer == 1)

### Example configuration fragment
These three filesets should cover everything under /data. (I recommend you check, unfortunately estimate will not help here unless you patch and rebuild bacula-fd.)
```
FileSet {
  Name = shard_one_of_three
  Include {
    Options {
      Signature = MD5
      Plugin = shard:0:3:0
    }
    File = /data
  }
}
FileSet {
  Name = shard_two_of_three
  Include {
    Options {
      Signature = MD5
      Plugin = shard:1:3:0
    }
    File = /data
  } 
}
FileSet {
  Name = shard_three_of_three
  Include {
    Options {
      Signature = MD5
      Plugin = shard:2:3:0
    }
    File = /data
  } 
}
```
