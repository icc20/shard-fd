/*
   Bacula(R) - The Network Backup Solution

   Copyright (C) 2000-2015 Kern Sibbald

   The original author of Bacula is Kern Sibbald, with contributions
   from many others, a complete list can be found in the file AUTHORS.

   You may use this file and others of this release according to the
   license defined in the LICENSE file, which includes the Affero General
   Public License, v3.0 ("AGPLv3") and some additional permissions and
   terms pursuant to its AGPLv3 Section 7.

   This notice must be preserved when any source code is 
   conveyed and/or propagated.

   Bacula(R) is a registered trademark of Kern Sibbald.
*/
#define BUILD_PLUGIN
#define BUILDING_DLL            /* required for Windows plugin */

#include <libgen.h>
#include "bacula.h"
#include "fd_plugins.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PLUGIN_LICENSE      "AGPLv3"
#define PLUGIN_AUTHOR       "Ian Clark"
#define PLUGIN_DATE         "May 2019"
#define PLUGIN_VERSION      "1"
#define PLUGIN_DESCRIPTION  "Pick 1 in n files based on filename hash"

#define FNV32a_BASIS 0x811c9dc5
#define FNV32a_PRIME 16777619

/* We want to use system strdup and free not the bacula ones */
#undef strdup
#undef free

/* Forward referenced functions */
static bRC newPlugin(bpContext *ctx);
static bRC freePlugin(bpContext *ctx);
static bRC getPluginValue(bpContext *ctx, pVariable var, void *value);
static bRC setPluginValue(bpContext *ctx, pVariable var, void *value);
static bRC handlePluginEvent(bpContext *ctx, bEvent *event, void *value);
static bRC startBackupFile(bpContext *ctx, struct save_pkt *sp);
static bRC endBackupFile(bpContext *ctx);
static bRC pluginIO(bpContext *ctx, struct io_pkt *io);
static bRC startRestoreFile(bpContext *ctx, const char *cmd);
static bRC endRestoreFile(bpContext *ctx);
static bRC createFile(bpContext *ctx, struct restore_pkt *rp);
static bRC setFileAttributes(bpContext *ctx, struct restore_pkt *rp);
static bRC checkFile(bpContext *ctx, char *fname);

/* Pointers to Bacula functions */
static bFuncs *bfuncs = NULL;
static bInfo  *binfo = NULL;

static pInfo pluginInfo = {
   sizeof(pluginInfo),
   FD_PLUGIN_INTERFACE_VERSION,
   FD_PLUGIN_MAGIC,
   PLUGIN_LICENSE,
   PLUGIN_AUTHOR,
   PLUGIN_DATE,
   PLUGIN_VERSION,
   PLUGIN_DESCRIPTION,
};

static pFuncs pluginFuncs = {
   sizeof(pluginFuncs),
   FD_PLUGIN_INTERFACE_VERSION,

   /* Entry points into plugin */
   newPlugin,                         /* new plugin instance */
   freePlugin,                        /* free plugin instance */
   getPluginValue,
   setPluginValue,
   handlePluginEvent,
   startBackupFile,
   endBackupFile,
   startRestoreFile,
   endRestoreFile,
   pluginIO,
   createFile,
   setFileAttributes,
   checkFile
};

/*
 * Plugin called here when it is first loaded
 */
bRC DLL_IMP_EXP
loadPlugin(bInfo *lbinfo, bFuncs *lbfuncs, pInfo **pinfo, pFuncs **pfuncs)
{
   bfuncs = lbfuncs;                  /* set Bacula funct pointers */
   binfo  = lbinfo;
   printf("shard-fd: Loaded: size=%d version=%d\n", bfuncs->size, bfuncs->version);

   *pinfo  = &pluginInfo;             /* return pointer to our info */
   *pfuncs = &pluginFuncs;            /* return pointer to our functions */

   return bRC_OK;
}

/*
 * Plugin called here when it is unloaded, normally when
 *  Bacula is going to exit.
 */
bRC DLL_IMP_EXP
unloadPlugin() 
{
   return bRC_OK;
}

/*
 * Called here to make a new instance of the plugin -- i.e. when
 *  a new Job is started.  There can be multiple instances of
 *  each plugin that are running at the same time.  Your
 *  plugin instance must be thread safe and keep its own
 *  local data.
 */
static bRC newPlugin(bpContext *ctx)
{
   /* Don't need this: 
   int JobId = 0;
   bfuncs->getBaculaValue(ctx, bVarJobId, (void *)&JobId); 
   
    * Docs say this is unimplemented: 
   bfuncs->registerBaculaEvents(ctx, 1, 2, 0); */
   //bfuncs->JobMessage(ctx,__FILE__,__LINE__,M_WARNING,0,"Shard plugin activated");
   return bRC_OK;
}

/*
 * Release everything concerning a particular instance of a 
 *  plugin. Normally called when the Job terminates.
 */
static bRC freePlugin(bpContext *ctx)
{
   /* 
   int JobId = 0;
   bfuncs->getBaculaValue(ctx, bVarJobId, (void *)&JobId);
   printf("plugin: freePlugin JobId=%d\n", JobId);
    */
   return bRC_OK;
}

/*
 * Called by core code to get a variable from the plugin.
 *   Not currently used.
 */
static bRC getPluginValue(bpContext *ctx, pVariable var, void *value) 
{
// printf("plugin: getPluginValue var=%d\n", var);
   return bRC_OK;
}

/* 
 * Called by core code to set a plugin variable.
 *  Not currently used.
 */
static bRC setPluginValue(bpContext *ctx, pVariable var, void *value) 
{
// printf("plugin: setPluginValue var=%d\n", var);
   return bRC_OK;
}

/*
 * Called by Bacula when there are certain events that the
 *   plugin might want to know.  The value depends on the
 *   event.
 */
static bRC handlePluginEvent(bpContext *ctx, bEvent *event, void *value)
{
   switch (event->eventType) {
   case bEventJobStart:
   case bEventJobEnd:
   case bEventStartBackupJob:
   case bEventEndBackupJob:
   case bEventLevel:
   case bEventSince:
   case bEventStartRestoreJob:
   case bEventEndRestoreJob:
   case bEventEstimateCommand:
   case bEventRestoreCommand:
   case bEventBackupCommand:
   case bEventPluginCommand:
   case bEventComponentInfo:
      break;

   /* handed a save_pkt structure in theory... */
   case bEventHandleBackupFile:
      {
      struct save_pkt *sp;
      char *cfg;
      uint32_t bucket,buckets,hashdirname;
      uint32_t hash= FNV32a_BASIS;
      char *path,*origpath;

      sp=(struct save_pkt *)value;

      // get config from cmd
      cfg=strchr(sp->cmd,':')+1;
      if(cfg==NULL) {
	bfuncs->JobMessage(ctx,__FILE__,__LINE__, M_FATAL, 0, "shard-fd command is incorrect\n");
	return bRC_Error;
      }
      bucket=strtol(cfg,NULL,10);
      cfg=strchr(cfg,':')+1;
      if(cfg==NULL) {
	bfuncs->JobMessage(ctx,__FILE__,__LINE__, M_FATAL, 0, "shard-fd command is incorrect\n");
	return bRC_Error;
      }
      buckets=strtol(cfg,NULL,10);
      if(bucket >= buckets) {
	bfuncs->JobMessage(ctx,__FILE__,__LINE__, M_FATAL, 0, "shard-fd configured to use an impossible bucket\n");
      }
      cfg=strchr(cfg,':')+1;
      if(cfg==NULL) {
	bfuncs->JobMessage(ctx,__FILE__,__LINE__, M_FATAL, 0, "shard-fd command is incorrect\n");
	return bRC_Error;
      }
      hashdirname=strtol(cfg,NULL,10);

      origpath=path=strdup(sp->fname);
      if(origpath==NULL) {
	bfuncs->JobMessage(ctx,__FILE__,__LINE__, M_FATAL, 0, "shard-fd unable to allocate memory\n");
	return bRC_Error;
      }

      // should probably save all dirs, this info should be in the sp somewhere.
      if(sp->type == FT_DIREND) {
	      free(origpath);
	      return bRC_Core;
      // Will FNV-1a hash the full path or just the dirname, then return bRC_Skip or bRC_Core
      } else if(hashdirname==1) {
	      path=dirname(path); // alternatively, cut off the filename and just hash the dirname
      } 
      while(*path) {
	      hash ^= (uint32_t)*path++;
	      hash *= FNV32a_PRIME;
      }
      free(origpath);
      // now do something with the hash
      /* https://lemire.me/blog/2016/06/27/a-fast-alternative-to-the-modulo-reduction/ */
      if(((uint64_t)hash * (uint64_t)buckets >> 32) == bucket) {
        return bRC_Core;
      } else {
        return bRC_Skip;
      }
      break;
      }
   default:
     bfuncs->JobMessage(ctx,__FILE__,__LINE__, M_WARNING, 0, "shard-fd plugin received unknown event %d\n",event->eventType);
   }
   return bRC_OK;
}


/*
 * Called when starting to backup a file.  Here the plugin must
 *  return the "stat" packet for the directory/file and provide
 *  certain information so that Bacula knows what the file is.
 *  The plugin can create "Virtual" files by giving them a
 *  name that is not normally found on the file system.
 */
static bRC startBackupFile(bpContext *ctx, struct save_pkt *sp)
{
   // we don't do backups!
   return bRC_Error;
}

/*
 * Done backing up a file.
 */
static bRC endBackupFile(bpContext *ctx)
{ 
   return bRC_OK;
}

/*
 * Do actual I/O.  Bacula calls this after startBackupFile
 *   or after startRestoreFile to do the actual file 
 *   input or output.
 */
static bRC pluginIO(bpContext *ctx, struct io_pkt *io)
{
   io->status = 0;
   io->io_errno = 0;
   switch(io->func) {
   case IO_OPEN:
      printf("plugin: IO_OPEN\n");
      break;
   case IO_READ:
      printf("plugin: IO_READ buf=%p len=%d\n", io->buf, io->count);
      break;
   case IO_WRITE:
      printf("plugin: IO_WRITE buf=%p len=%d\n", io->buf, io->count);
      break;
   case IO_CLOSE:
      printf("plugin: IO_CLOSE\n");
      break;
   }
   return bRC_OK;
}

static bRC startRestoreFile(bpContext *ctx, const char *cmd)
{
   return bRC_OK;
}

static bRC endRestoreFile(bpContext *ctx)
{
   return bRC_OK;
}

/*
 * Called here to give the plugin the information needed to
 *  re-create the file on a restore.  It basically gets the
 *  stat packet that was created during the backup phase.
 *  This data is what is needed to create the file, but does
 *  not contain actual file data.
 */
static bRC createFile(bpContext *ctx, struct restore_pkt *rp)
{
   return bRC_OK;
}

/*
 * Called after the file has been restored. This can be used to
 *  set directory permissions, ...
 */
static bRC setFileAttributes(bpContext *ctx, struct restore_pkt *rp)
{
   return bRC_OK;
}

/* When using Incremental dump, all previous dumps are necessary */
static bRC checkFile(bpContext *ctx, char *fname)
{
   return bRC_OK;
}


#ifdef __cplusplus
}
#endif
