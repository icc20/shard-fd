CXX = /usr/bin/g++
LIBTOOL = ${BACDIR}/libtool
LIBTOOL_COMPILE = $(LIBTOOL) --silent --tag=CXX --mode=compile
LIBTOOL_LINK = $(LIBTOOL) --silent --tag=CXX --mode=link
LIBTOOL_INSTALL = $(LIBTOOL) --silent --tag=CXX --mode=install
LIBTOOL_INSTALL_FINISH = $(LIBTOOL) --silent --tag=CXX --finish --mode=install
LIBTOOL_UNINSTALL = $(LIBTOOL) --silent --tag=CXX --mode=uninstall
LIBTOOL_CLEAN = $(LIBTOOL) --silent --tag=CXX --mode=clean
DEFAULT_OBJECT_TYPE = .lo

SRCDIR = ${BACDIR}/src
FDDIR = ${SRCDIR}/filed


CFLAGS = -g -O2 -Wall -x c++ -fno-strict-aliasing -fno-exceptions -fno-rtti -I${SRCDIR} -I${FDDIR}

all: shard-fd.la

shard-fd.lo: shard-fd.c
ifndef BACDIR
	$(error BACDIR needs to be defined, see README.md)
endif
	$(LIBTOOL_COMPILE) $(CXX) $(DEFS) $(DEBUG) $(CPPFLAGS) $(CFLAGS) -I${SRCDIR} -I${FDDIR} -c shard-fd.c

shard-fd.la: Makefile shard-fd$(DEFAULT_OBJECT_TYPE)
ifndef BACDIR
	$(error BACDIR needs to be defined, see README.md)
endif
ifndef PLUGINDIR
	$(error PLUGINDIR needs to be defined, see README.md)
endif
	$(LIBTOOL_LINK) $(CXX) $(LDFLAGS) -shared shard-fd.lo -o $@ -shared -rpath $(PLUGINDIR) -module -export-dynamic -avoid-version

install: shard-fd.la
	$(LIBTOOL_INSTALL) install shard-fd.la $(PLUGINDIR)
ifndef PLUGINDIR
	$(error PLUGINDIR needs to be defined, see README.md)
endif

clean:
	rm -r shard-fd.la shard-fd.lo .libs

patch-estimate:
ifndef BACDIR
	$(error BACDIR needs to be defined, see README.md)
endif
	patch -d $(BACDIR) -p1 < bacula-estimate.patch

unpatch-estimate:
ifndef BACDIR
	$(error BACDIR needs to be defined, see README.md)
endif
	patch -R -d $(BACDIR) -p1 < bacula-estimate.patch
